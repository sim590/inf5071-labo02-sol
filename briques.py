#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw

size = (256,256)
xwidth = 32
ywidth = 16
xmargin = 2
ymargin = 2

image = Image.new("RGBA", size, color=(0,0,0,255))
draw = ImageDraw.Draw(image)
# xoffset = 0
# for y in range(0, 256 + ywidth, ywidth):
#     xoffset = - xwidth / 2 if xoffset == 0 else 0
#     for x in range(0, 256 + xwidth, xwidth):
#         rectangle = [(xoffset + x + xmargin, y + ymargin),\
#                      (xoffset + x + xwidth - xmargin, y + ywidth - ymargin)]
#         draw.rectangle(rectangle, fill=(255,0,0,255))

xoffset = 0
for y in range(0, size[1], ywidth):
    xoffset = - xwidth / 2 if xoffset == 0 else 0
    for x in range(0, size[0]+xwidth, xwidth):
        rectangle = [(xoffset + x + xmargin, y + ymargin),\
                     (xoffset + x + xwidth - xmargin, y + ywidth - ymargin)]
        draw.rectangle(rectangle, fill=(255,0,0,255))

image.save("briques.png", "PNG")

# vim:set et sw=4 ts=4 tw=120:

